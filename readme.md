# PMS 72 changes to default Bootstrap files

## Mondriaan Fonds logo located at:
Because PMS 72 can't change any html, the MF logo is saved as the old logo name.
```
D:\web\mfuat\www\images\aimslogo.png
```

## Fonts located at:
```
D:\web\mfuat\www\styles\fonts\lineto-akkurat-pro-*
```

## Custom style added to files located at:

```
D:\web\mfuat\www\styles\bootstrap.min.css
D:\web\mfuat\www\styles\bootstrap-theme.min.css
```
and
```
D:\web\mfuat\www\styles\external\bootstrap.min.css
D:\web\mfuat\www\styles\external\bootstrap-theme.min.css
```

## Changes to bootstrap.min.css start at line 18
And there are some more changes across this file. Most important are including the web font Akkurat and setting it to the `body` element like this (line 1115):
```
body {
  font-family: "Akkurat-Pro", "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-size: 14px;
  font-weight: normal;
  line-height: 1.42857143;
  color: #1a1a1a;
  background-color: #ffffff;
}
```

## Changes to bootstrap-theme.min.css start at line 418

```
/**
 * PMS 72 Custom styling
 */

/** Layout */
#register .form-register {
  border: 1px solid rgba(153, 153, 153, 0.5);
  ...............
```